﻿using Microsoft.AspNetCore.Mvc;
using BusMapApi.Lib.Db;
using BusMapApi.Lib.ActionFilter;
using BusMapApi.Lib.Entity;
using System.Linq;
using BusMapApi.Lib.Services;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace BusMapApi.Controllers
{
    [Route("api/[controller]")]
    public class BusRouteController : Controller
    {
        private IDbContext context;
        private IBusRouteService service;

        public BusRouteController(IDbContext context, IBusRouteService service)
        {
            this.context = context;
            this.service = service;
        }

        [HttpGet]
        [Paginate]
        public IQueryable<BusRoute> Browse()
        {
            return service.Collection.AsQueryable();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var result = await service.Find(id);
            return Ok(result);
        }
    }
}
