﻿namespace BusMapApi.Lib.Db
{
    using BusMapApi.Lib.Entity;
    using MongoDB.Driver;
    using System;

    public class DbContext : IDbContext
    {
        private MongoClient mongoClient;
        public IMongoDatabase Database { get; private set; }

        public IMongoCollection<BusRoute> BusRouteCollection { get; private set; }
        public IMongoCollection<BusStop> BusStopCollection { get; private set; }

        public DbContext(string connectionString, string databaseName)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentException($"{nameof(connectionString)} is null or whitespace");

            if (string.IsNullOrWhiteSpace(databaseName))
                throw new ArgumentException($"{nameof(databaseName)} is null or whitespace");

            var mongoUrlBuilder = new MongoUrlBuilder(connectionString);
            mongoClient = new MongoClient(mongoUrlBuilder.ToMongoUrl());

            Database = mongoClient.GetDatabase(databaseName);

            InitiateCollections();
            InitiateIndexes();
        }

        private void InitiateIndexes()
        {
            var createIndexOptions = new CreateIndexOptions()
            {
                Background = true
            };

            BusStopCollection.Indexes.CreateOne(Builders<BusStop>.IndexKeys.Geo2DSphere(x => x.Location));
        }

        private void InitiateCollections()
        {
            BusRouteCollection = this.Database.GetCollection<BusRoute>(CollectionNames.BusRoute);
            BusStopCollection = this.Database.GetCollection<BusStop>(CollectionNames.BusStops);
        }
    }
}
