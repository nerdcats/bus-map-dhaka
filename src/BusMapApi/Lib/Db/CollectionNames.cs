﻿namespace BusMapApi.Lib.Db
{
    internal class CollectionNames
    {
        public const string BusRoute = "routes";
        public const string BusStops = "stops";
    }
}