﻿using BusMapApi.Lib.Entity;
using MongoDB.Driver;

namespace BusMapApi.Lib.Db
{
    public interface IDbContext
    {
        IMongoCollection<BusRoute> BusRouteCollection { get; }
        IMongoCollection<BusStop> BusStopCollection { get; }
        IMongoDatabase Database { get; }
    }
}