﻿namespace BusMapApi.Lib.Model
{
    using System.Collections.Generic;

    public class LineString
    {
        public List<float[]> coordinates { get; set; }
        public string type { get; set; }
    }
}
