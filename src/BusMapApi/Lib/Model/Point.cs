﻿namespace BusMapApi.Lib.Model
{
    public class Point
    {
        public float[] coordinates { get; set; }
        public string type { get; set; }
    }
}
