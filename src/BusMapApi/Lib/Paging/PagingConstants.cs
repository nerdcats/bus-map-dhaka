﻿namespace BusMapApi.Lib.Paging
{
    public class PagingConstants
    {
        public const int MaxPageSize = 25;
    }
}
