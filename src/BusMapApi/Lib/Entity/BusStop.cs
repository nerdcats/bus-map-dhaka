﻿namespace BusMapApi.Lib.Entity
{
    using BusMapApi.Lib.Model;
    using MongoDB.Bson.Serialization.Attributes;

    [BsonIgnoreExtraElements(true)]
    public class BusStop: DbEntity
    {
        public string Name { get; set; }
        public Point Location { get; set; }
        public string Type { get; set; }
    }
}
