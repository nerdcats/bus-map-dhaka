﻿using BusMapApi.Lib.Model;
using MongoDB.Bson.Serialization.Attributes;

namespace BusMapApi.Lib.Entity
{
    [BsonIgnoreExtraElements(true)]
    public class BusRoute: DbEntity
    {
        public string BusName { get; set; }
        public string RouteNo { get; set; }
        public string StartPoint { get; set; }
        public string EndPoint { get; set; }
        public float RouteLength { get; set; }
        public LineString Route { get; set; }
    }
}
