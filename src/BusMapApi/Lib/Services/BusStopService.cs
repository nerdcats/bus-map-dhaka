﻿using BusMapApi.Lib.Db;
using BusMapApi.Lib.Entity;
using BusMapApi.Lib.Exception;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace BusMapApi.Lib.Services
{
    public class BusStopService : IBusStopService
    {
        public IMongoCollection<BusStop> Collection { get; private set; }

        public BusStopService(IDbContext dbContext)
        {
            this.Collection = dbContext.BusStopCollection;
        }

        public async Task<BusStop> Create(BusStop busStop)
        {
            if (busStop == null)
                throw new ArgumentNullException(nameof(busStop));

            await this.Collection.InsertOneAsync(busStop);
            return busStop;
        }

        public async Task<BusStop> Find(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentException(nameof(id));

            var result = await this.Collection.Find(x => x.Id == id).FirstOrDefaultAsync();
            if (result == null)
                throw new EntityNotFoundException(typeof(BusStop), id);

            return result;
        }

        public async Task<BusStop> Delete(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentException(nameof(id));

            var result = await this.Collection.FindOneAndDeleteAsync(x => x.Id == id);
            if (result == null)
                throw new EntityDeleteException(typeof(BusStop), id);

            return result;
        }

        public async Task<BusStop> Update(BusStop busStop)
        {
            if (busStop == null)
                throw new ArgumentNullException(nameof(busStop));

            if (string.IsNullOrWhiteSpace(busStop.Id))
                throw new ArgumentNullException(nameof(busStop.Id));

            var result = await this.Collection.FindOneAndReplaceAsync(x => x.Id == busStop.Id, busStop);
            if (result == null)
                throw new EntityUpdateException(typeof(BusStop), busStop.Id);

            return result;
        }
    }
}
