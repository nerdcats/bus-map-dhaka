﻿using BusMapApi.Lib.Db;
using BusMapApi.Lib.Entity;
using BusMapApi.Lib.Exception;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace BusMapApi.Lib.Services
{
    public class BusRouteService : IBusRouteService
    {
        public IMongoCollection<BusRoute> Collection { get; private set; }

        public BusRouteService(IDbContext dbContext)
        {
            this.Collection = dbContext.BusRouteCollection;
        }

        public async Task<BusRoute> Create(BusRoute busRoute)
        {
            if (busRoute == null)
                throw new ArgumentNullException(nameof(busRoute));

            await this.Collection.InsertOneAsync(busRoute);
            return busRoute;
        }

        public async Task<BusRoute> Find(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentException(nameof(id));

            var result = await this.Collection.Find(x => x.Id == id).FirstOrDefaultAsync();
            if (result == null)
                throw new EntityNotFoundException(typeof(BusRoute), id);

            return result;
        }

        public async Task<BusRoute> Delete(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentException(nameof(id));

            var result = await this.Collection.FindOneAndDeleteAsync(x => x.Id == id);
            if (result == null)
                throw new EntityDeleteException(typeof(BusRoute), id);

            return result;
        }

        public async Task<BusRoute> Update(BusRoute busRoute)
        {
            if (busRoute == null)
                throw new ArgumentNullException(nameof(busRoute));

            if (string.IsNullOrWhiteSpace(busRoute.Id))
                throw new ArgumentNullException(nameof(busRoute.Id));

            var result = await this.Collection.FindOneAndReplaceAsync(x => x.Id == busRoute.Id, busRoute);
            if (result == null)
                throw new EntityUpdateException(typeof(BusRoute), busRoute.Id);

            return result;
        }
    }
}
