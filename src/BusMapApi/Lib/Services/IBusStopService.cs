﻿using System.Threading.Tasks;
using BusMapApi.Lib.Entity;
using MongoDB.Driver;

namespace BusMapApi.Lib.Services
{
    public interface IBusStopService
    {
        IMongoCollection<BusStop> Collection { get; }

        Task<BusStop> Create(BusStop busStop);
        Task<BusStop> Delete(string id);
        Task<BusStop> Find(string id);
        Task<BusStop> Update(BusStop busStop);
    }
}