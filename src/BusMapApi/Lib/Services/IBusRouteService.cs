﻿using System.Threading.Tasks;
using BusMapApi.Lib.Entity;
using MongoDB.Driver;

namespace BusMapApi.Lib.Services
{
    public interface IBusRouteService
    {
        IMongoCollection<BusRoute> Collection { get; }

        Task<BusRoute> Create(BusRoute busRoute);
        Task<BusRoute> Delete(string id);
        Task<BusRoute> Find(string id);
        Task<BusRoute> Update(BusRoute busRoute);
    }
}