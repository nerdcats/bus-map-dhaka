﻿namespace BusMapDatabaseMaker
{
    public class BusStop
    {
        public string Name { get; set; }
        public Location Location { get; set; }
        public string Type { get; set; }
    }

    public class Location
    {
        public float Longitude { get; set; }
        public float Latitude { get; set; }
    }
}
