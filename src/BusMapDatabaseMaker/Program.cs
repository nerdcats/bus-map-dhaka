﻿namespace BusMapDatabaseMaker
{
    using MongoDB.Bson;
    using MongoDB.Driver;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.IO;

    public class Program
    {
        static void Main(string[] args)
        {
            var mongoUrlBuilder = new MongoUrlBuilder("mongodb://nerdcats:welovecats@45.76.9.217:27017/busmap");
            var mongoClient = new MongoClient(mongoUrlBuilder.ToMongoUrl());

            var database = mongoClient.GetDatabase("busmap");
            var routeCollection = database.GetCollection<RouteModel>("routes");
            var busStopCollection = database.GetCollection<Stop>("stops");

            database.DropCollection("routes");
            database.DropCollection("stops");

            var routes = JsonConvert.DeserializeObject<List<RouteModel>>(File.ReadAllText(@"RouteData.json"));

            foreach (var item in routes)
            {
                routeCollection.InsertOne(item);
            }

            var stops = JsonConvert.DeserializeObject<List<BusStop>>(File.ReadAllText(@"BusStops.json"));

            foreach (var item in stops)
            {
                busStopCollection.InsertOne(Stop.FromBusStop(item));
            }
        }
    }
}
