﻿using GeoJSON.Net.Geometry;
using MongoDB.Bson;
using Newtonsoft.Json;

namespace BusMapDatabaseMaker
{
    public class Stop
    {
        public string Name { get; set; }
        public BsonDocument Location { get; set; }
        public string Type { get; set; }

        public static Stop FromBusStop(BusStop data)
        {
            var point = new GeographicPosition(
                data.Location.Latitude,
                data.Location.Longitude);


            return new Stop()
            {
                Location = BsonDocument.Parse(JsonConvert.SerializeObject(new Point(point))),
                Name = data.Name,
                Type = data.Type,
            };
        }
    }
}
