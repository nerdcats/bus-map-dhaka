﻿using GeoJSON.Net.Geometry;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Linq;

namespace BusMapDatabaseMaker
{
    public class RouteModel
    {
        [JsonProperty(PropertyName = "bus_name")]
        public string BusName { get; set; }

        [JsonProperty(PropertyName = "route_no")]
        public string RouteNo { get; set; }

        [JsonProperty(PropertyName = "start_point")]
        public string StartPoint { get; set; }

        [JsonProperty(PropertyName = "end_point")]
        public string EndPoint { get; set; }

        [JsonProperty(PropertyName = "route_length")]
        public float RouteLength { get; set; }

        [BsonRequired]
        public BsonDocument Route
        {
            get
            {
                var points = this.Path.Select(x => new GeographicPosition(x.Latitude, x.Longitude));
                var lineString = new LineString(points);
                return BsonDocument.Parse(JsonConvert.SerializeObject(lineString));
            }
        }

        [BsonIgnore]
        public Path[] Path { get; set; }

        public bool ShouldSerializePath()
        {
            return false;
        }
    }

    public class Path
    {
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}
