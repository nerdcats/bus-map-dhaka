﻿

namespace BusMap.Model
{
    using System.Collections.Generic;
    using System.Device.Location;
    using BusMap.Messages_And_Helpers;

    internal class RouteData
    {
        public string bus_name { get; set; }
        public string route_no { get; set; }

        public string start_point { get; set; }
        public string end_point { get; set; }

        public double route_length { get; set; }

        public List<GeoCoordinate> Path { get; set; }
        public List<string> Stopages { get; set; }

        public RouteData()
        {
            Stopages = new List<string>();
            Path = new List<GeoCoordinate>();
        }
    }

    internal class RouteDataCollection
    {
        private static RouteDataCollection _instance;
        public static RouteDataCollection Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RouteDataCollection();
                }
                return _instance;
            }
            
        }

        private List<RouteData> _allRouteData;
        public  List<RouteData> AllRouteData 
        {
            get { return _allRouteData; }
            private set { this._allRouteData = value; } 
        }

        private RouteDataCollection(){}

        public void GetAllRouteData()
        {
            AllRouteData = JsonHelper.ParseJsonToList<RouteData>(ResourceFileNames.RouteData);           
        }



    }
}
