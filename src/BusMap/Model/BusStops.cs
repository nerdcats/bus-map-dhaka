﻿
namespace BusMap.Model // Had to go for singleton to ensure only once xml access. No multiple access
{
    using System.Device.Location;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using BusMap.Messages_And_Helpers;
    using BusMap;
    using GalaSoft.MvvmLight;
    using System.IO.IsolatedStorage;
    using System.Windows;
    using System.Linq;

    internal class BusStops : ObservableObject
    {
        private static BusStops _instance; 
        private IsolatedStorageSettings _Settings;


        public static BusStops Instance
        {

            get { return _instance ?? (_instance = new BusStops()); }
        }
        private ObservableCollection<BusStop> _AllBusStops = null;

        public ObservableCollection<BusStop> AllBusStops { get { return _AllBusStops; } set { _AllBusStops = value; RaisePropertyChanged("AllBusStops"); } }

        private BusStops()
        {
        }

        private void SyncFavourites(ObservableCollection<BusStop> favList)
        {
            try
            {
                var favInAllBusStops = from fav in favList
                                       join busStop in AllBusStops
                                       on fav.Name equals busStop.Name
                                       select busStop;
                foreach (var item in favInAllBusStops)
                {
                    item.isFavorite = true;
                }
            }
            catch
            { }            
        }



        public void GetAllBusStops() //Have to make it async
        {
            AllBusStops = new ObservableCollection<BusStop>(JsonHelper.ParseJsonToList<BusStop>(ResourceFileNames.BusStops));
            _Settings = IsolatedStorageSettings.ApplicationSettings;

            if(_Settings.Contains(Settings.IsFavoritesAvailable))
            {
                ObservableCollection<BusStop> FavList = (ObservableCollection<BusStop>)_Settings[Settings.IsFavoritesAvailable];
                SyncFavourites(FavList);
               
            }
            
        }
    }

    public class BusStop : ObservableObject
    {
        private bool _isVisible = true;
        private bool _isFavorite = false;

        public string Name { get; set; }
        public GeoCoordinate Location { get; set; }
        public string Type { get; set; }
        public List<string> BusNames { get; set; }
        public bool isVisible { get { return _isVisible; } set { _isVisible = value; RaisePropertyChanged("isVisible"); } } //very very very bad practice, for the sake of time and feature I have to do it. Would do something to refactor this later
        public bool isFavorite { get { return _isFavorite; } set { _isFavorite = value; RaisePropertyChanged("isFavorite"); } }
    }
}
