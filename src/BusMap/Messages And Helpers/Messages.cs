﻿
namespace BusMap
{
    using GalaSoft.MvvmLight.Messaging;

    internal class InvokeMethodMessage
    {
        public string MethodName { get; set; }
        public object[] Parameters { get; set; }
    }

    internal class EnableDisableControlMessage
    {
        public string ControlName { get; set; }
        public System.Windows.Visibility ControlVisibility { get; set; }
        public bool isFocussed { get; set; }
    }

    internal static class MessageHelper
    {
        public static void SendMessageToView<T>(T Message) where T: new()
        {
            Messenger.Default.Send<T>(Message);
        }
    }
}
