﻿namespace BusMap.Messages_And_Helpers
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal static class JsonHelper
    {
        internal static List<T> ParseJsonToList<T>(string resourceFileName)
        {
            JsonSerializer Serializer = new JsonSerializer();
            object RecievedData;

            using (StreamReader SReader = new StreamReader("Resources/"+resourceFileName))
            using (JsonTextReader JReader = new JsonTextReader(SReader))
            {
                RecievedData = Serializer.Deserialize(JReader);
            }            

           return (from item in RecievedData as JArray
                        select JsonConvert.DeserializeObject<T>(item.ToString())).ToList<T>();
            
        }
    }
}
