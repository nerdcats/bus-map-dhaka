﻿using System.Device.Location;
namespace BusMap
{
    internal class Methods
    {
        public const string SetMapView = "SetMapView";
        public const string ShowProgressIndicator = "ShowProgressIndicator";
        public const string HideProgressIndicator = "HideProgressIndicator";
        public const string DrawRoute = "DrawRoute";
        public const string RemoveRoute = "RemoveRoute";
        public const string ZoomOntoBusStop = "ZoomOntoBusStop";
        public const string DrawAccuracyRadius = "DrawAccuracyRadius";
    }

    internal class ResourceFileNames
    {
        public const string BusStops = "BusStops.txt";
        public const string RouteData = "RouteData.txt";
    }

    internal class StylesAndTemplates
    {
        public const string MaximizedPushpin = "MaximizedPushpin";
        public const string CustomizedPushpin = "CustomizedPushpin";
    }

    internal enum Parameters
    {
        Location, Accuracy 
    }

    internal class Controls
    {
        public const string LocationPanel = "LocationPanel";
        public const string SearchTextBoxTo = "SearchTextBoxTo";
        public const string SearchTextBoxFrom = "SearchTextBoxFrom";
    }

    internal class Properties
    {
        public const string BusStopLocations = "BusStopLocations";
        public const string BusRoutes = "BusRoutes";
        public const string MyLocation = "MyLocation";
    }

    internal class Settings
    {
        public const string LastCoordinate = "LastCoordinate";
        public const string IsLocationAllowed = "IsLocationAllowed";
        public const string IsFavoritesAvailable = "IsFavoritesAvailable";
        public const string IsFirstRun = "IsFirstRun";
    }

    internal static class DefaultLocations
    {
        public static GeoCoordinate _DefaultLocation = new GeoCoordinate() { Latitude = 23.72825, Longitude = 90.41112 };
        public static GeoCoordinate DefaultLocation { get { return _DefaultLocation; } }
    }

    internal class Constants
    {
        public const int maxFavNumber = 15;
        public const int maxNearestStops = 5;
        public const int maxRadius = 1500;
        public const double GeoMovementThreshhold=100;
        public const int DefaultAccuracy = 50;
        public const int AccuracyRadiusConstraint = 180;

       
    }
}
