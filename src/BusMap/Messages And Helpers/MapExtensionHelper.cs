﻿namespace BusMap
{
    using Microsoft.Phone.Maps.Controls;
using Model;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Windows;
    

    internal static class MapExtensionHelper
    {
        private static readonly BusStops busStops= BusStops.Instance;
        private static readonly RouteDataCollection RouteCollection = RouteDataCollection.Instance;


      
        //The following are seriously bugged as distance should be collected from map service. 
        public static List<BusStop> GetNearestBusStopByRadius(GeoCoordinate myPosition, double distanceRadiusInMeters = Constants.maxRadius)
        {
            


            return busStops != null || busStops.AllBusStops.Count > 0
                       ? busStops.AllBusStops.OrderBy(x=>x.Location.GetDistanceTo(myPosition)).TakeWhile(s => s.Location.GetDistanceTo(myPosition) < distanceRadiusInMeters).ToList()
                       : null;
        }

        public static List<BusStop> GetNearestBusStop(GeoCoordinate myPosition, int count = Constants.maxNearestStops)
        {
            return busStops != null && busStops.AllBusStops.Count > 0
                ? busStops.AllBusStops.OrderBy(s => s.Location.GetDistanceTo(myPosition)).Take(Constants.maxNearestStops).ToList()
                : null;
        }


        //Generic Search, DETERMINISTIC

        public static List<Path> SearchRouteGenereic(BusStop Source, BusStop Destination)
        {
            List<RouteData> query = null;
            List<Path> ReturnPathCollection = null;

            try
            {
                //Find paths that has the source and destination BusStop
                query = (from BusRoute in RouteCollection.AllRouteData
                         where BusRoute.Stopages.Contains(Source.Name) && BusRoute.Stopages.Contains(Destination.Name)
                         select BusRoute).ToList();

                if (query != null && query.Count > 0)
                {

                  

                    //redundant piece of code, needs to be optimized
                    


                    //intializing ReturnPathColelction
                    ReturnPathCollection=new List<Path>();

                    //cutting the path
                    
                    foreach (var FoundRoute in query)
                    {
                        var tempPath = FoundRoute.Path;
                        var tempStopages = FoundRoute.Stopages;

                        int CutIndex1 = tempPath.IndexOf(tempPath.OrderBy(s => s.GetDistanceTo(Source.Location)).First());
                        int CutIndex2 = tempPath.IndexOf(tempPath.OrderBy(s => s.GetDistanceTo(Destination.Location)).First());

                        tempPath = CutIndex1 > CutIndex2 ? tempPath.GetRange(CutIndex2, CutIndex1 - CutIndex2) : tempPath.GetRange(CutIndex1, CutIndex2 - CutIndex1);

                        int CutIndex3 = tempStopages.IndexOf(Source.Name);
                        int CutIndex4 = tempStopages.IndexOf(Destination.Name);

                        tempStopages = CutIndex3 > CutIndex4 ? tempStopages.GetRange(CutIndex4, (CutIndex3 - CutIndex4)+1) : tempStopages.GetRange(CutIndex3, (CutIndex4 - CutIndex3)+1);

                        
                        
                        Path AddPath = new Path() { BusName = FoundRoute.bus_name, legs = tempPath , Stopages=tempStopages};

                        if (Destination.Location.GetDistanceTo(AddPath.legs.First()) > Destination.Location.GetDistanceTo(AddPath.legs.Last()))
                        {
                            AddPath.legs.Add(Destination.Location);
                            AddPath.legs.Insert(0,Source.Location);
                        }
                        else
                        {
                            AddPath.legs.Insert(0, Destination.Location);
                            AddPath.legs.Add(Source.Location);
                        }

                        double distance = 0.0;
                        //should do the following loop in Linq
                        for (int count = 0; count < AddPath.legs.Count - 2; count++)
                        {
                            distance = distance + AddPath.legs[count].GetDistanceTo(AddPath.legs[count + 1]);
                        }

                        AddPath.Distance = distance/1000;


                        
                       
                        ReturnPathCollection.Add(AddPath);

                    }                  
                }
            }
            catch(Exception ex)
            { }


            return SortPath(ReturnPathCollection);

        }
        public static List<Path> concatenatePath(List<Path> ReturnData, BusStop DestinationStop)
        {
            foreach (var path in ReturnData)
            {
                if (DestinationStop.Location.GetDistanceTo(path.legs.First()) > DestinationStop.Location.GetDistanceTo(path.legs.Last()))
                    path.legs.Add(DestinationStop.Location);
                else
                    path.legs.Insert(0, DestinationStop.Location);

            }

            return ReturnData;

        }

        private static List<Path> SortPath(List<Path> path)
        {
            if (path != null && path.Count > 0)
            {
                path = path.OrderBy(x => x.Stopages.Count).ToList();
            }

            return path;
        }

        //Function that searches destination routes to a predefined bus stop
        public static List<Path> SearchForRoute(GeoCoordinate myLocation, BusStop DestinationStop)
        {
            
            List<BusStop> NearestBusStops = GetNearestBusStopByRadius(myLocation);
            List<Path> ReturnData = null; //Have to make this a list
            List<Path> ReturnPath = null;
            
            
            foreach (var BusStop in NearestBusStops)
            {
                try
                {
                 

                    ReturnData = SearchRouteGenereic(BusStop, DestinationStop);
                    if (ReturnData != null && ReturnData.Count > 0)
                    {
                        if (ReturnPath == null)
                            ReturnPath = new List<Path>();
                        ReturnPath=ReturnPath.Concat(ReturnData).ToList(); // Have to create a good filter here.
                    }

                }
                catch (Exception ex)
                { continue;}
            }

            
            return  ReturnPath;           
        }

        public static LocationRectangle GetLocationRectangle(List<GeoCoordinate> Legs)
        {
            return LocationRectangle.CreateBoundingRectangle(Legs);
        }
    }

    internal class Path
    {
        public List<GeoCoordinate> legs { get; set; }
        public string BusName { get; set; }
        public List<string> Stopages { get; set; }
        public double Distance { get; set; }
    }
}
